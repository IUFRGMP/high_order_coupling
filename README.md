The method makes use of the Fourier fitting for high dimensional data using the Eigen library.

THe coupling coefficients are determined by optimal fitting of Fourier coefficients using singular value decomposition.

The method can be used for arbitrarily many oscillators and arbitrarily many Fourier modes.

compilation is intended to be:

g++ -std=c++11 -O -o FFit Main.cpp

Usage is:

FFit experiment number - datafile.dat - modes - nosc - pcols - dcols

modes: specifies the maximum integer (mode) used in one direction of fitting;--- nosc: number of oscillatory units to be fitted;-- pcols: all phase columns in data file;--- dcols: all derivative columns (in same ordering a pcols!) in the data file

 