/* ########################################## */
/*  FOURIER FITTING OF COUPLING COEFFICIENTS  */
/* Author: Erik Gengel, University of Potsdam */
/* ########################################## */

#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <cmath>
#include <sstream>
#include <ostream>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include "../HEADERFILES_PHASE_COUPLING/eigen/Eigen/Dense"        //for linear algebra 

#define onlyzero
//#define ordered

using namespace Eigen;

typedef Matrix<double,Dynamic,Dynamic> MatD;
typedef Matrix<double,Dynamic,1> VecD;

using namespace std;

//### Dtype for precission controlle ###//
typedef double DOUB ;

const DOUB pi(atan(1.0)*4.0); //pi

//### DEFINITION OF MAIN DATA STRUCTURE ###
typedef struct Vectordata
{

  //### Parsing of data ###// 
  vector<int> phasecol;      //true phase columns from 1 (0 is time, actuay the first col index is 3 for one oscillator)
  vector<int> derivecol;      //derivative columns

  vector<DOUB> phase;       //calculated phase

  vector<DOUB> derive;       //calculated derivatives

}Vectordata;


//### FUNCTION DEFS ###

void extractor(Vectordata &Data , string &datafile)//<long double> &data2, vector<long double> &timeGrid)//TODO: design of code!!!
 {
  cout << "reading a file" << "\n";
  string line;
  ifstream myfile(datafile);
  ifstream myfile2 (datafile);
  //### EXTRACT FILE CONTENT OF FORM data <SPACE> time \n to struct members ###//
  int n(0);
  if(myfile.is_open())
   {
    while(getline(myfile,line))//determine length of data lines=#time steps
    { 
     cout << "while 1\n"; 
     n+=1;
    }
    Data.phase.resize(Data.phasecol.size()*n);
    Data.derive.resize(Data.phasecol.size()*n);
    myfile.close();
   }
  if(myfile2.is_open())
   {
    cout << "if2 \n";
    DOUB a(0.0);
    int kk(0);//line
    while(getline(myfile2,line))//dataGen<number>.dat -> scan data file for lines
     {
      cout << "while 2\n";
      std::stringstream ss(line);
      istringstream is(line);
      int jj=0;
      while(is >> a)//scan line for columns
       {
	for(int k=0;k<Data.phasecol.size();k++)//iterate through oscillators
	 { 
	  //cout << "If jj== " << jj << " , a= " << a << " pcol-1= " << Data.phasecol[k]-1 << " derivecol-1= " << Data.derivecol[k]-1 << "\n";
          if(jj==Data.phasecol[k]-1)
           {
            Data.phase[n*k+kk]=a;
            //cout << "If jj== " << jj << " , a= " << a << "\n";
	    //cout << "hit: p_" << Data.phasecol[k]-1 << "= a= " << a << " = p_vec= " << Data.phase[n*k+kk] << "vecind= " << n*k+kk << "\n";
           }
	  if(jj==Data.derivecol[k]-1)
           {
            Data.derive[n*k+kk]=a;
            //cout << "If jj== " << jj << " , a= " << a << "\n";
            //cout << "hit: dp_" << Data.phasecol[k]-1 << "= a= " << a << " = dp_vec= " << Data.derive[n*k+kk] << "vecind= " << n*k+kk << "\n";
           }
	 }
        jj +=1;
       }//off column scan
      kk +=1;
     }//off line scan-> time iterator
    myfile2.close();
   }//off open file
 }

void indexfill(vector<vector<DOUB>> &modes, int &n, int &q, int N, int M)
 {
  if(n<N-1)
   {  
    cout << "if: n= " << n << "\n";
    q=-M-1;
    int incr(1);
    for(int k=0;k<N-n-1;k++){incr *=(2*M+1);}//set modulo increment for tree depth level
    cout << "incr= " << incr << "\n";
    for(int k=0; k<modes.size(); k++)
     {
      if(k%incr==0){q+=1;}
      if(q>M){q=-M;}
      modes[k][n]=DOUB(q);
     }
    n+=1;
    indexfill(modes, n, q, N, M);
   }
  else
   {    
    cout << "end depth= " << n << "\n";
    q=-M;
    for(int k=0; k<modes.size(); k++)
     {
      if(q>M){q=-M;}
      modes[k][n]=DOUB(q);
      q+=1;
     }
   }
 }

int main(int argc, char* argv[])
{
  //### GET ARGVALS ###//
  DOUB in = strtod(argv[1],NULL);    // distinguish file parameter , argv[0] own name
  string datafile = argv[2];         // file if readed
  int M = int(strtod(argv[3],NULL)); // #Fourier modes in one direction of dimension for fitting
  int N= int(strtod(argv[4],NULL));  //# oscillatory units
  
  //### INITIALIZE DATA STRUCT ###//
  Vectordata Data;

  for(int k=5;k<5+N;k++)//read in phase cols
   {
    Data.phasecol.push_back(int(strtod(argv[k],NULL)));
   }
  for(int k=5+N;k<5+2*N;k++)//read in derive cols
   {
    Data.derivecol.push_back(int(strtod(argv[k],NULL)));
   }

  //### EXTRAC FILE CONTENT ###//
  
  extractor(Data, datafile);
  
  /*cout << "datasize= " << Data.phase.size() << "\n";
  for(int k=0;k<Data.phase.size();k++)
   {
    cout << " k= " << k << " phi= "  << Data.phase[k] << " derive= " << Data.derive[k] << "\n"; 
   }
  cout << "finished \n";//*/

  
  //### SET FOURIER INDEX VECTORS COMBINATIONS ###//
  vector<vector<DOUB>> modes_pre(pow(2*M+1,N),vector<DOUB>(N,0));
  cout << "len modes=" << modes_pre.size() << "\n";
  for(int k=0; k<modes_pre.size();k++)
   {
    for(int l=0; l<N; l++)
     {
      cout << " k,l= " << k << " , " << l << " " << modes_pre[k][l];
     }
    cout << "\n";
   }//*/
 
 int n(0), q(-M);
 indexfill(modes_pre, n, q, N, M);//generate tree
 
#ifdef onlyzero
 vector<vector<DOUB>> modes;
 DOUB mt(0.0);
 for(int k=0; k<modes_pre.size();k++)
  {
   mt=0.0;
   for(int j=0; j<N; j++)
    {
     mt += modes_pre[k][j];
    }
   if(mt==0.0){modes.push_back(modes_pre[k]);}
  }
#else
 vector<vector<DOUB>> modes(pow(2*M+1,N),vector<DOUB>(N,0));
 modes=modes_pre;
#endif

 cout << "after indexfill \n";
  for(int k=0; k<modes.size();k++)
   {
    for(int l=0; l<N; l++)
     {
      cout << modes[k][l] << " " ;
     }
    cout << "\n";
   }//*/

  //### REMOVE COMPLEX CONJUGATES ###//
  /*vector<vector<DOUB>> modes_pre(pow(2*M+1,N),vector<DOUB>(N,0));
  modes_pre=modes;
  DOUB check(0.0);
  vector<DOUB> mod_ref(N);
  for(int k=0; k<modes.size(); k++)//iterate through all modes
   {
    mod_ref=modes[k];//copy ref mode
    for(int l=0; l<modes.size();l++)//compare other modes to ref mode
     { 
      check=0.0;
      if(l!=k)//avoid self comparison
       {
        for(int m=0;m<N;m++)//check each entry of tupel
	 {
          if(mod_ref[m]==-modes[l][m]){check +=1.0;}
	 }
	if(check==N)//if all entries are complex conjugate
	 {
          for(int m=0; m<N; m++)//these entries in copy of original set to zero
	   {
            modes_pre[l][m] = 0.0;
	   }
	 }
       }//off avoid
     }//off compare
   }//off iterate

  //### RESIZE ###//
  int zero_count(0), k_pre(0);
  for(int k=0; k<modes_pre.size(); k++)//iterate through all modes
   {
    check=0.0;
    for(int l=0; l<modes_pre[0].size();l++){check += modes_pre[k_pre][l]*modes_pre[k_pre][l];}
    if(check>0.0){modes[k]=modes_pre[k_pre];}
    else{k_pre +=1; zero_count +=1;}
    k_pre +=1;
   }
//*/
  // modes.resize(modes.size()/2+1);//remove all zero entries but leave one there for constant term

  cout << "after removal \n";
  for(int k=0; k<modes.size();k++)
   {
    for(int l=0; l<N; l++)
     {
      cout << modes[k][l] << " " ;
     }
    cout << "\n";
   }//*/


//####################################################################
  //### CALCULATE MATRIX ELEMENTS ###//
  int L(Data.phase.size()/N);//determine length in time
  //cout << "L= " << L << "\n";
  //cout << modes.size() << "\n";
  MatD CS(L,2*modes.size());//matrix contains cos terms, sin terms and the constant term, moeds contains 0 term, so subtract one of those 
  DOUB scalprod(0.0);
  for(int l=0; l<L; l++)//iterate in time
   {
    for(int k=0;k<modes.size();k++)//iterate in mode combinations, cos
     {
      scalprod=0.0;
      for(int m=0; m<modes[0].size();m++)
       {
	//cout << "p= " << Data.phase[m*L+l];
	//cout << "modes= " << modes[k][m] << "\n";
        scalprod += modes[k][m]*Data.phase[m*L+l];
	//cout << "scalprod= " << scalprod << "\n";
        //cout << "modes[" << k << "][" << m << "]= " << modes[k][m] << " p= " << Data.phase[m*L+l] << " scalprod= " << scalprod << "\n";
       }
      //cout << "cos= " << cos(scalprod) << "\n";
      CS(l,k)=cos(scalprod);
      //cout << "CS[" << l << "][" << k << "]= " << CS(l,k) << "\n";
      //cout << CS << "\n";
     }
    for(int k=0;k<modes.size();k++)//iterate in mode combinations, sin
     {
      scalprod=0.0;
      for(int m=0; m<modes[0].size();m++)
       {
	//cout << "p= " << Data.phase[m*L+l];
        //cout << "modes= " << modes[k][m] << "\n";
        scalprod += modes[k][m]*Data.phase[m*L+l];
        //cout << "scalprod= " << scalprod << "\n";
        //cout << "modes[" << k << "][" << m << "]= " << modes[k][m] << " p= " << Data.phase[m*L+l] << " scalprod= " << scalprod << "\n";
       }
      //cout << "sin= " << sin(scalprod) << "\n";
      CS(l,modes.size()+k)=sin(scalprod);
      //cout << "CS[" << l << "][" << modes.size()+k << "]= " << CS(l,modes.size()+k) << "\n";
     }//*/
   }

  /*cout << "################\n";
  for(int k=0; k<L; k++)
   { 
    for(int kk=0;kk<2*modes.size();kk++)
     {
      cout << CS(k,kk) << " ";
     }
    cout << "\n";
   }//*/

  //### FIND OPTIMAL COEFFICIENTS ###//
  //cout << "#### \n";
  for(int k=0;k<N;k++)
   {
    VecD dPk(L);
    for(int l=0;l<L;l++)
     {
      dPk(l)=Data.derive[k*L+l];
     }
    VecD Coeff(2*modes.size());                                //solve now: dPk = CS*Coeff for Coeff
    Coeff = CS.bdcSvd(ComputeThinU | ComputeThinV).solve(dPk); //solve syste with singular value decomposition
    //Coeff = CS.fullPivHouseholderQr().solve(dPk); //solve  system with QR decomposition
    //order coefficients
    vector<vector<DOUB>> modes_ordered(pow(2*M+1,N),vector<DOUB>(N,0));
    VecD Coeff_ordered(2*modes.size());
    int modes_index(0);
    DOUB maxval(0.0);
    DOUB maxval_before(10000.0);
#ifdef ordered
    for(int q=0;q<modes.size();q++)
     {
      modes_index=0;
      maxval=sqrt(Coeff(0)*Coeff(0)+Coeff(modes.size())*Coeff(modes.size()));
      //cout << "q= " << q << "\n";
      for(int p=1;p<modes.size();p++)
       {
        if((sqrt(Coeff(p)*Coeff(p)+Coeff(modes.size()+p)*Coeff(modes.size()+p))>maxval))
	 {
	  if(sqrt(Coeff(p)*Coeff(p)+Coeff(modes.size()+p)*Coeff(modes.size()+p))<maxval_before)
	   {
	    maxval=sqrt(Coeff(p)*Coeff(p)+Coeff(modes.size()+p)*Coeff(modes.size()+p)); 
	    modes_index=p; 
            //cout << "if: found maxval: "<< maxval << " before " << maxval_before << "\n";
	   }
         }
       }
      maxval_before=maxval;
      //cout << "maxval_before= " << maxval_before << "\n";
      //cout << "modes_index= " << modes_index << "\n";
      modes_ordered[q]=modes[modes_index];//combination
      //cout << "mod_o= " << modes_ordered[q][0] << modes_ordered[q][1] << modes_ordered[q][2] << " mod= "  << modes_ordered[modes_index][0] << modes_ordered[modes_index][1] << modes_ordered[modes_index][2] << "\n";
      Coeff_ordered(q)=Coeff(modes_index);//cos
      //cout << "COF_ord cos= " << Coeff_ordered(q) << " COF cos = " << Coeff(modes_index) << "\n";
      Coeff_ordered(modes.size()+q) = Coeff(modes.size()+modes_index);//sin
      //cout << "COF_ord sin= " << Coeff_ordered(modes.size()+q) << " COF sin= " << Coeff(modes.size()+modes_index) << "\n";
     }
#else
modes_ordered=modes;
Coeff_ordered=Coeff;
#endif

    //output coeffs
    std::stringstream out0a; out0a << "Coeffs" << in << "." << k << ".dat";
    std::string name0a = out0a.str(); ofstream fouta(name0a,ios::out);
    fouta.setf(ios::scientific); fouta.precision(15);
    for(int l=0;l<modes.size();l++)
     {
      fouta << l << " ";
      for(int m=0;m<N;m++)
       {
        fouta << modes_ordered[l][m] << " ";
       }
      fouta << Coeff_ordered(l) << " " << Coeff_ordered(modes.size()+l) << "\n";
     }
    fouta.close();
   
    //output real+reconstructed coupling
    std::stringstream out0b; out0b << "Derive" << in << "." << k << ".dat";
    std::string name0b = out0b.str(); ofstream foutb(name0b,ios::out);
    foutb.setf(ios::scientific); foutb.precision(15);
    for(int l=0;l<L;l++)//time
     {
      DOUB dPrec(0.0),scalprod(0.0);
      for(int m=0;m<modes.size();m++)//sun over mult index
       {
	    scalprod=0.0;
    	for(int n=0; n<modes[0].size(); n++)//scalar product
	     {
          scalprod += modes_ordered[m][n]*Data.phase[L*n+l];
	     }
        dPrec += Coeff_ordered(m)*cos(scalprod) + Coeff_ordered(modes.size()+m)*sin(scalprod);
       }
      for(int m=0; m<N;m++)
       {
        foutb << Data.phase[m*L+l] << " ";
       }
      foutb << dPrec << " " << Data.derive[k*L+l] << "\n";
     }
    foutb.close();
   } //*/

  return 0;
}
